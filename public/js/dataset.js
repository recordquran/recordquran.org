import { dataset_meta } from "./service.js";
import { set_date } from "./languages.js";

let last_modified = document.getElementById('last-update-date');

function on_dataset_meta_available(dataset_meta)
{
    let last_modified_date = new Date(dataset_meta.last_modified);

    last_modified.dataset.day = last_modified_date.getDate();
    last_modified.dataset.month = last_modified_date.getMonth() + 1;
    last_modified.dataset.year = last_modified_date.getFullYear();

    set_date(last_modified);
}

dataset_meta(on_dataset_meta_available);