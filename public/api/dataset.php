<?php

require_once "config.php";

use \Google\Cloud\Storage\StorageClient;

/**
 * List Cloud Storage bucket objects.
 *
 * @param string $bucketName the name of your Cloud Storage bucket.
 *
 * @return void
 */
function object($projectId, $bucketName)
{
    $config = [
        'projectId' => $projectId,
    ];

    $storage = null;

    try
    {
        $storage = new StorageClient($config);
    }
    catch(Exception $e)
    {
        error_log("Could not create StorageClient: {$e->getMessage()}");
        echo("500 internal server error");
        http_response_code(500);
        die();
    }

    
    
    $bucket = $storage->bucket($bucketName);

    $object = $bucket->object('archive/archive.txt');

    return $object;
}

function dataset_meta()
{
    $archive_object = object(project_name(), bucket_name());
    $info = $archive_object->info();

    // whenever we update the archive we delete the old
    // on and create a new one
    // so the timeCreated is the last_modification
    echo json_encode(array('last_modified' => $info['timeCreated']), JSON_FORCE_OBJECT);
    http_response_code(200);
}