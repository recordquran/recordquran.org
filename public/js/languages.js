export {
  change_language,
  populate_numbers_dropdown,
  populate_suras,
  finished_recording_message,
  upload_in_progress_message,
  arabic_digits,
  available_languages,
  convertStr2Num,
  set_date,
  set_lang_from_url,
};

const local_storage = window.localStorage;

const arabicNumsToEnglish = (s) =>
  s.replace(/[٠-٩]/g, (d) => "٠١٢٣٤٥٦٧٨٩".indexOf(d));

const language_arabic = {
  sura: " اسم السورة:",
  aya: "رقم الآية:",
  record: "سجل",
  stop: "إيقاف",
  next: "التالية",
  prev: "السابقة",
  dataset: "حمل الأصوات",
  record_my_voice: "سجل صوتي!",
  no_data: "الموقع حديث، لا يوجد تسجيلات بعد :( زرنا لاحقاً.",
  downloadTitle: "حمل القراءات",
  datasetPara:
    "من هنا يمكنك تحميل جميع القرائات التي سجلها الناس على الموقع لاستخدامها في الأغراض البحثية .",
  downloadAll: "حمل كل القرائات",
  lastUpdatePara: `اخر تحديث بتاريخ`,
  greetings: "اهلا بك",
  greetings_para1:
    "يسجل الموقع قراءتك للقرآن ليساعد البحث والتطوير للبرامج المتعلقة بالقرآن الكريم. ",
  greetings_para2: "معلومات بسيطة عنك ستساعد في تحسين جودة التطوير.",
  age: "العمر:",
  toggleAge: "فتح وغلق جدول الأعمار",
  age_options: "إختر أو اكتب عمرك",
  ageError: "يجب أن يكون بين ٤ و ١٠٠",
  gender: "النوع:",
  male: "ذكر",
  female: "أنثى",
  tongue: "اللغة الأم:",
  toggleTongue: "فتح وغلق جدول اللغات",
  tongue_options: "إختر اللغة",
  tongueError: "يجب أن يكون من القائمة المتوفرة",
  begin: "ابدأ الآن",
  // navbar
  mobToggleNavbar: "فتح وغلق شريط التنقل",
  startReciting: "إبدأ التلاوة",
  dataset: "حمل القراءات",
  about_link: "ما هذا الموقع؟",

  just_a_sec: "لحظة...",
  after_recording_message:
    "جزاك الله خيراً، لقد سجلت {}. وفي المجمل سجلت {}. عُد غداً واقرأ مثلهم.",
  upload_in_progress_message:
    "جاري رفع التسجيلات ({}/{})، رجاءً لا تغلق الموقع.",
  beforRecord: "اضغط لتسجيل تلاوتك",
  whileRecord: "جاري التسجيل…",
  zero: arabic_digits("00"),

  toggleAya: "فتح وغلق جدول الآيات",
  toggleSura: "فتح وغلق جدول السور",
  search: "ابحث",
  ayaSearch: "البحث برقم الآية",
  suraSearch: "البحث باسم السورة",

  direction: "rtl",
  sura_list: "suras_ar",
  logo_src: "./images/logo-ar.svg",
  about_us_page_link: "./about_ar.html",

  digits_conversion: arabic_digits,
  ayah_word: ayah_word_arabic,

  month_1: "يناير",
  month_2: "فبراير",
  month_3: "مارس",
  month_4: "ابريل",
  month_5: "مايو",
  month_6: "يونيو",
  month_7: "يوليو",
  month_8: "أغسطس",
  month_9: "سبتمبر",
  month_10: "أكتوبر",
  month_11: "نوفمبر",
  month_12: "ديسمبر",

  why_not_zip: `
    <h3>
    زر التحميل يأتيني بقائمة من الروابط للملفات منفردة، كنت أتوقع أن أحمل ملف واحد.
    </h3>
    <br/>
    إذا كنت لا تعلم كيفية تحميل هذه الملفات راسلني وسأعد إليك ملف zip وأبعثه إليك
    وهذا بريدي:
    <br/>
    
    <a class="bold contact_link" >adhamzahranfms@gmail.com</a>

    <br/>
    <br/>
    
    <h3>
    ولكن لماذا؟ 
    </h3>
    <br/>
    لسبب تقنى، إذا كان هناك ملف zip يحتوي على كل التسجيلات فذلك يعني أننا سنخزن
    كل التسجيلات مرتين. وهو أمر مقبول إذا كان عدد التسجيلات قليل، ولكن مع توسع الموقع بإذن الله
    سيزداد الأمر صعوبة وتكلفة.
    `,

  dataset_usage: `زر التحميل يأتيك بملف يعدد كل الروابط للتسجيلات.
    أسماء الملفات تتبع الصيغة الآتية:
    <strong>uuid_age_mothertongue_gender_sura_aya.opus</strong>
    <br/>
    مثال علي اسم ملف:
    <strong>5fe4ca4013618_f65378b2-d391-4096-8094-7aae0a9f969b_27_ar-EG_m_2_35.opus</strong>

    <ul class="list wrapper-list">
        <li>حيث أن uuid رمز خاص لكل قارئ
            <ul class="list inner-list">
                <li>مجموعةً من الأحرف والأرقام بينها شرطة (-) لا تتكرر بين قارئ وآخر،
                في المثال أعلاه ال uuid هو:
                <br>
                <strong>5fe4ca4013618_f65378b2-d391-4096-8094-7aae0a9f969b</strong>
                </li> 
            </ul>
        </li>
         
        <li>age هو عمر القارئ وفي المثال عمره 
        <strong>27</strong></li>
        <li>وال mothertongue هو الخلفية اللغوية للقارئ
            <ul class="list inner-list">
                <li>وهي حرفان يعبران عن اللغة تبقاً ل ISO-639 متبوعة بشرطة
                </li>
                <li>ثم حرفان يعبران عن البلد طبقاً ل ISO-3166
                </li> 
            </ul>

            في المثال الخلفية اللغوية هي <strong>ar-EG</strong> وتعني عربي، مصر
        </li>
        
        <li>وال gender هو نوع القارئ كونه ذكر أو كونها أنثى
            <ul class="list inner-list">
                <li>فيرمز للذكر ب m</li>
                <li>ويرمز للأنثى ب f</li> 
            </ul>
            في المثال القارئ
            <strong> ذكر</strong>
        </li>

        <li>sura هو رقم السورة، في المثال هي سورة
         <strong>٢</strong>
         ،وهي سورة البقرة
         
         </li>
        <li>و aya هو رقم الآية، في المثال هي آية 
        <strong>٣٥</strong>
        </li>
    </ul>
    `,
};

const language_english = {
  sura: "Sura:",
  aya: "Aya:",
  record: "Record",
  stop: "Stop",
  next: "Next",
  prev: "Previous",
  dataset: "Dataset",
  record_my_voice: "Record my voice!",
  no_data:
    "The website is very new, we still don't have any recordings :( Come back soon.",
  downloadTitle: "Download the recordings",
  downloadAll: "Download all recordings",
  lastUpdatePara: `Last update on`,
  greetings: "Welcome",
  greetings_para1:
    "This website records your voice reciting the holy Quran to assist the research and development of Holy Quran related applications.",
  greetings_para2:
    "Anonymous information about you that will help the research.",
  age: "Age:",
  toggleAge: "Toggle Age tabel",
  age_options: "Choose your age",
  ageError: "Must be between 4 and 100",
  gender: "Gender:",
  male: "Male",
  female: "Female",
  tongue: "Mother Tongue:",
  toggleTongue: "toggle Tongue tabel",
  tongue_options: "Choose the language",
  tongueError: "Must be from the options in the list",
  begin: "Begin",
  // navbar
  mobToggleNavbar: "Toggle navbar links' list",
  startReciting: "Start Reciting",
  dataset: "Dataset",
  about_link: "What is this website?",

  just_a_sec: "Just a second...",
  after_recording_message:
    "God bless. You've recorded {}. Your total recordings are {}. Please come back tomorrow and recite some more.",
  upload_in_progress_message:
    "Uploading ayat ({}/{}), please don't close this window yet.",
  beforRecord: "Click to record your recitation",
  whileRecord: "Recording In Progress…",
  zero: "00",

  toggleAya: "Toggle Ayat tabel",
  toggleSura: "Toggle sura tabel",
  search: "Search",
  ayaSearch: "Search by the aya number",
  suraSearch: "Search by the name of the sura",

  direction: "ltr",
  sura_list: "suras_en",
  logo_src: "./images/logo.svg",
  about_us_page_link: "./about_en.html",

  digits_conversion: null,
  ayah_word: ayah_word_english,

  month_1: "January",
  month_2: "February",
  month_3: "March",
  month_4: "April",
  month_5: "May",
  month_6: "June",
  month_7: "July",
  month_8: "August",
  month_9: "September",
  month_10: "October",
  month_11: "November",
  month_12: "December",

  why_not_zip: `
    <h3>
    I was expecting to download a single zip file, not a list of links to audio files
    </h3>
    <br/>

    If you don't know how to download all of these files
    email me and I'll make an archive for you and send you a link.
    <br/>
    
    <a class="bold contact_link" >adhamzahranfms@gmail.com</a>

    <br/>
    <br/>
    
    <h3>Why though?</h3>
    <br/>

    Sadly, that's due to a technical limitation; making
    an archive of all of the data means that we'll always have to store the data twice, which is
    feasible with a small number of recordings, but as we scale this will become more and more
    unpractical.`,

  dataset_usage: `The download button will download a file that has a 
    list of all the Quran recitations. The names of the audio 
    files are formated as follows:
    <strong>uuid_age_mothertongue_gender_sura_aya.opus</strong>
    
    Here's an example file name:
    <strong>5fe4ca4013618_f65378b2-d391-4096-8094-7aae0a9f969b_27_ar-EG_m_2_35.opus</strong>

    <ul class="list wrapper-list">
        <li>uuid is a unique identifier to the reciter.
            <ul class="list inner-list">
                <li>It is group of characters and digits separated by hyphens, together they're guaranteed to be unique for each reciter, 
                in the example above, the uuid is: <br> <strong>5fe4ca4013618_f65378b2-d391-4096-8094-7aae0a9f969b</strong></li> 
            </ul>
        </li>
         
        <li>Then there is the age of the reciter, which is <strong>27</strong> in our example</li>
        <li>then there is the reciter's 'mothertongue' which is the linguistic background
            <ul class="list inner-list">
                <li>which is a 2-letter ISO-639 language code followed by a hyphen</li>
                <li>then a 2-letter ISO-3166 country code</li> 
            </ul>
            In the example above the linguistic background is <strong>ar-EG</strong> which means
            Arabic, Egypt.
        </li>
        
        <li>Then there's the gender
            <ul class="list inner-list">
                <li>which is either m for male</li>
                <li>or f for female</li> 
            </ul>
            In the example the reciter is <strong>male</strong>
        </li>

        <li>Lastly there is the number of the sura which is <strong>2</strong> or Al-Baqara in our example</li>
        <li>then the number of the aya which is <strong>35</strong> in the example</li>
    </ul>`,
};

const available_languages = {
  ar: language_arabic,
  en: language_english,
};

function ayah_word_arabic(number_of_ayat) {
  if (number_of_ayat == 1) {
    return "آية واحدة";
  }

  if (number_of_ayat == 2) {
    return "آيتين";
  }

  if (number_of_ayat >= 3 && number_of_ayat <= 10) {
    return "{} آيات";
  }

  return "{} آية";
}

function ayah_word_english(number_of_ayat) {
  if (number_of_ayat == 1) {
    return "{} Ayah";
  }

  return "{} Ayat";
}

function finished_recording_message(
  number_of_ayat_today,
  total_number_of_ayat
) {
  const lang = local_storage.getItem("lang");

  var message = available_languages[lang].after_recording_message;

  const digits_conversion = available_languages[lang].digits_conversion;

  let ayah_word_today;
  let ayah_word_total;

  if (digits_conversion === null) {
    ayah_word_today = available_languages[lang].ayah_word(number_of_ayat_today);
    ayah_word_today = ayah_word_today.replace("{}", number_of_ayat_today);

    ayah_word_total = available_languages[lang].ayah_word(total_number_of_ayat);
    ayah_word_total = ayah_word_total.replace("{}", total_number_of_ayat);
  } else {
    ayah_word_today = available_languages[lang].ayah_word(number_of_ayat_today);
    ayah_word_today = ayah_word_today.replace(
      "{}",
      digits_conversion(number_of_ayat_today)
    );

    ayah_word_total = available_languages[lang].ayah_word(total_number_of_ayat);
    ayah_word_total = ayah_word_total.replace(
      "{}",
      digits_conversion(total_number_of_ayat)
    );
  }

  message = message.replace("{}", ayah_word_today);
  message = message.replace("{}", ayah_word_total);
  return message;
}

function upload_in_progress_message(uploaded_recordings, recordings_count) {
  const lang = local_storage.getItem("lang");

  var message = available_languages[lang].upload_in_progress_message;

  const digits_conversion = available_languages[lang].digits_conversion;

  if (digits_conversion === null) {
    message = message.replace("{}", uploaded_recordings);
    return message.replace("{}", recordings_count);
  } else {
    message = message.replace("{}", digits_conversion(recordings_count));
    return message.replace("{}", digits_conversion(uploaded_recordings));
  }
}

function place_text(el, config) {
  const key = el.getAttribute("data-i18n");
  // innerText replaced with innerHtml
  el.innerHTML = config[key];
}

function place_direction(el, config) {
  el.style.direction = config.direction;
}

function arabic_digits(en_digits) {
  const map = [
    "&#1632;",
    "&#1633;",
    "&#1634;",
    "&#1635;",
    "&#1636;",
    "&#1637;",
    "&#1638;",
    "&#1639;",
    "&#1640;",
    "&#1641;",
  ];

  var ar_digits = "";

  en_digits = String(en_digits);

  for (var i = 0; i < en_digits.length; i++) {
    ar_digits += map[parseInt(en_digits.charAt(i))];
  }

  return ar_digits;
}

function populate_numbers_dropdown(dropdown, range_start, range_end) {
  const lang = local_storage.getItem("lang");

  const digits_conversion = available_languages[lang].digits_conversion;

  dropdown.innerHTML = "";

  for (var i = range_start; i <= range_end; i++) {
    let num = digits_conversion ? digits_conversion(i) : i;
    dropdown.innerHTML += generateHtmlTemplete(i, num);
  }

  // UI for ayat
  const currentPath = window.location.pathname;
  if (currentPath.includes("index")) {
    const firstAya = document.querySelector(`.aya-number[data-value='1']`);
    firstAya.classList.add("selected");
  }
}

function generateHtmlTemplete(i, num) {
  const currentPath = window.location.pathname;
  if (currentPath.includes("register")) {
    return (
      '<div class="d-none" onclick="onClick_onageDiv(event)" data-value="' +
      i +
      '">' +
      num +
      "</div>"
    );
  } else {
    return (
      '<p class="aya-number tabel-options" onclick="onSelectAyaFromDropDownClicked(this.dataset.value)" data-value="' +
      i +
      '">' +
      num +
      "</p>"
    );
  }
}
function convertDigitFromEnToAR(enDigit, lang) {
  // PERSIAN, ARABIC, URDO

  if (lang === "ar") {
  let newValue = "";

    console.log(lang);
    const arabicNumbers =
      "\u0660\u0661\u0662\u0663\u0664\u0665\u0666\u0667\u0668\u0669";
    newValue = new String(enDigit).replace(/[0123456789]/g, (d) => {
      return arabicNumbers[d];
    });
    return newValue;
  }
  return enDigit;
}
function populate_suras(lang) {
  const sura = document.getElementById("sura-tabel-content");

  sura.innerHTML = "";

  let p = "";

  for (var i = 1; i <= 114; i++) {
    let sura_name = eval("suras_" + lang + "[" + i + "]");
    p +=
      '<p class="sura-name tabel-options" onclick="onSelectSuraFromDropDown(this.dataset.value)" data-value="' +
      i +
      '">' +
      convertDigitFromEnToAR(i, lang) +
      ". " +
      sura_name +
      "</p>";
  }

  sura.innerHTML = p;
}

function set_date(el) {
  let day_ = el.dataset.day;
  let month_ = el.dataset.month;
  let year_ = el.dataset.year;

  if (!day_ || !month_ || !year_) {
    return;
  }

  let day = number_in_lang(day_);
  let month = month_name(month_);
  let year = number_in_lang(year_);

  el.innerHTML = day + " " + month + " " + year;
}

function change_language(lang) {
  if (lang == null || lang == "") {
    lang = "ar";
  }

  local_storage.setItem("lang", lang);

  let language = available_languages[lang];

  const translated_elements = document.querySelectorAll("[data-i18n]");
  translated_elements.forEach((el) => place_text(el, language));

  const date_elements = document.querySelectorAll("[data-date]");
  date_elements.forEach((el) => set_date(el));

  const dir_elements = document.querySelectorAll("[dir]");
  dir_elements.forEach((el) => place_direction(el, language));

  // change logo
  const logo = document.getElementById("logo");
  if (logo) {
    logo.src = language.logo_src;
  }

  // change the href of the "about us" link
  if (document.querySelector(".aboutus-link")) {
    document.querySelectorAll(".aboutus-link").forEach((link) => {
      link.href = language.about_us_page_link;
    });
  }
  return lang;
}

function convertStr2Num(value) {
  let val;
  if (/[٠-٩]/g.test(value)) {
    val = parseInt(arabicNumsToEnglish(value));
  } else {
    val = parseInt(value);
  }
  return val;
}

function month_name(month_number) {
  const lang = local_storage.getItem("lang");

  return available_languages[lang]["month_" + month_number];
}

function number_in_lang(number) {
  const lang = local_storage.getItem("lang");

  if (!available_languages[lang].digits_conversion) {
    return number;
  }

  return available_languages[lang].digits_conversion(number);
}

function get_url_vars() {
  var vars = {};
  window.location.href.replace(
    /[?&]+([^=&]+)=([^&]*)/gi,
    function (m, key, value) {
      vars[key] = value;
    }
  );
  return vars;
}

function set_lang_from_url() {
  var l = get_url_vars()["lang"];

  if (l != null) {
    var found = false;

    for (const language in available_languages) {
      if (language == l) {
        local_storage.setItem("lang", language);
        break;
      }
    }
  }
}

set_lang_from_url();

window.change_language = change_language;
change_language(local_storage.getItem("lang"));

// prevent the addition of # to the url
document.querySelectorAll('.lang-switch a[href="#"]').forEach((a) => {
  a.addEventListener("click", (e) => e.preventDefault());
});
