
import { change_language, populate_numbers_dropdown, convertStr2Num } from "./languages.js";

const local_storage = window.localStorage;

var age = local_storage.getItem('age');
var gender = local_storage.getItem('gender');
var tongue = local_storage.getItem('tongue');

local_storage.setItem('sura', 1);
local_storage.setItem('aya', 1);

var ageList = document.getElementById('age-list');
var tongueList = document.getElementById('tongue-list');
// -------- for list events --------
let isTongueListOpen = false;
let isAgeListOpen = false;
let ageListCurrentShown = [];
let tongueListCurrentShown = [];
// -------- for list events --------

// -------- to validate the tounge lang ---------
const tounge_lang_pairs = { size: 0 };
const toungue_lang_set = new Set();
document.querySelectorAll('#tongue-list > div').forEach(elem => {
    // this to check of values like "af-za": ["Afrikaans", "South Africa"];
    let lang = elem.querySelector('div');
    tounge_lang_pairs.size++;
    let text = lang.innerText.replace(/ /g,'').toLocaleLowerCase();
    tounge_lang_pairs[text] = elem.dataset.value.toLowerCase();

    // we need to check also for the code only like (ar-eg) so, we will use a set
    toungue_lang_set.add(elem.dataset.value.toLowerCase());
});
// -------- to validate the tounge lang ---------


function create_uuid() {
    // http://www.ietf.org/rfc/rfc4122.txt
    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
    s[8] = s[13] = s[18] = s[23] = "-";

    var uuid = s.join("");
    return uuid;
}

// all the waves you contribute would be assigned a unique id
// so that a researchers would know that all of these audio files
// come from the same person
local_storage.setItem("id", create_uuid());

function populate_age() {
    populate_numbers_dropdown(ageList, 4, 100);
    beginBtnBgColor();
}

function _on_change_language(lang) {
    change_language(lang);
    populate_age();

    age = local_storage.getItem('age');
    gender = local_storage.getItem('gender');
    tongue = local_storage.getItem('tongue');

    if (age) document.getElementById("age").value = local_storage.getItem('lang') === 'ar' ? parseInt(age).toLocaleString('ar-EG') : age;;
    if (document.querySelector(`[data-value='${age}']`)) document.querySelector(`[data-value='${age}']`).classList.add('checked');
    document.getElementById('male').checked = gender === 'm';
    document.getElementById('female').checked = gender === 'f';
    initToungeInputVal();

    // make sure that lists are closed correctly
    isTongueListOpen = false;
    isAgeListOpen = false;
    closeList(tongueList.parentElement);
    closeList(ageList.parentElement);

}


// ----------------------------------
// init
// ----------------------------------

populate_age();

window._on_tongue_change = _on_tongue_change;
window._on_gender_change = _on_gender_change;
window._on_age_change = _on_age_change;
window._on_change_language = _on_change_language;
window.onClick_onageDiv = onClick_onageDiv;

if (age) document.getElementById("age").value = local_storage.getItem('lang') === 'ar' ? parseInt(age).toLocaleString('ar-EG') : age;
if (document.querySelector(`[data-value='${age}']`)) document.querySelector(`[data-value='${age}']`).classList.add('checked');
document.getElementById('male').checked = gender === 'm';
document.getElementById('female').checked = gender === 'f';

initToungeInputVal();
function initToungeInputVal() {
    if (tongue) {
        tongue = `${tongue.split('-')[0].toLowerCase()}${tongue.split('-')[1] ? '-' + tongue.split('-')[1].toUpperCase() : ''}`;
        if (document.querySelector(`[data-value='${tongue}']`)) {
            let tongueText = document.querySelector(`[data-value='${tongue}'] > div`).innerText;
            document.querySelector(`[data-value='${tongue}']`).classList.add('checked');
            document.getElementById("tongue").value = tongueText;
        }
    }
}


// ---------------------------------
// UI
// ---------------------------------

// hide the span (that do as a placeholder)
if (age) document.getElementById('age-placeholder-span').classList.add('d-none');
if (tongue) document.getElementById('tongue-placeholder-span').classList.add('d-none');

// validation for begin btn
const registerForm = document.getElementById('register-form');
registerForm.addEventListener('submit', e => {
    e.preventDefault();
    if (local_storage.getItem('age') && local_storage.getItem('gender') && local_storage.getItem('tongue')) {
        window.location.href = "./index.html";
    }
})
function beginBtnBgColor() {
    const beginBtn = document.getElementById('begin-btn');
    if (local_storage.getItem('age') && local_storage.getItem('gender') && local_storage.getItem('tongue')) {
        beginBtn.classList.add('bg-main-color');
    } else {
        beginBtn.classList.remove('bg-main-color');
    }
}
beginBtnBgColor();

// -----------------------------------
// events of the age and tounge inputs
// -----------------------------------

// ---- 1- toggle list events
document.getElementById("toggle-age-list").addEventListener('click', toggleList);
document.getElementById("toggle-tongue-list").addEventListener('click', toggleList);

function toggleList(e) {
    let parent = this.parentElement;
    let otherList = parent === tongueList.parentElement ? ageList.parentElement : tongueList.parentElement;
    let list = parent.querySelector('.list-toggle-btn').id;

    if (list === 'toggle-tongue-list') {
        isTongueListOpen = !isTongueListOpen;
        isAgeListOpen = false;
    } else if (list === 'toggle-age-list') {
        isAgeListOpen = !isAgeListOpen;
        isTongueListOpen = false;
    }
    const forCheck = list === 'toggle-age-list' ? isAgeListOpen : isTongueListOpen;

    if (forCheck) {
        closeList(otherList);
        openList(parent);
        showTheListItems(parent);
    } else {
        closeList(parent);
    }
}

// ---- 2- on focus
document.getElementById('tongue').addEventListener('focus', e => {
    isTongueListOpen = true;
    isAgeListOpen = false;
    closeList(ageList.parentElement);
    openList(tongueList.parentElement);
    showTheListItems(tongueList.parentElement);
});

document.getElementById('age').addEventListener('focus', e => {
    isAgeListOpen = true;
    isTongueListOpen = false;
    closeList(tongueList.parentElement);
    openList(ageList.parentElement);
    showTheListItems(ageList.parentElement);
});

// ---- 3- on blur 
document.getElementById('tongue').addEventListener('blur', e => {
    setTimeout(() => {
        if (!local_storage.getItem('tongue')) {
            toggleErrorMessage('tongue', 'add');
            beginBtnBgColor();
        }
    }, 200);
})

document.getElementById('age').addEventListener('blur', () => {
    setTimeout(() => {
        if(!local_storage.getItem('age')) toggleErrorMessage('age', 'add');
    }, 200);
})

// ---- 4- close the list when click on the body itself
let ids = new Set([
    'age',
    'toggle-age-list',
    'tongue',
    'toggle-tongue-list'
])
document.body.addEventListener('click', e => {
    if (!ids.has(e.target.id) && !ids.has(e.target.parentElement.id)) {
        isTongueListOpen = false;
        isAgeListOpen = false;
        closeList(document.getElementById('tongue-list').parentElement);
        closeList(document.getElementById('age-list').parentElement);
    }
});

// ---- 5- on input change

function _on_age_change(e, value) {
    if (e.code !== 'Tab' && e.code !== 'Escape' && e.code !== 'ArrowUp' && e.code !== 'ArrowDown') {
        if (value.trim()) {

            // hide the span (that do as a placeholder)
            document.getElementById('age-placeholder-span').classList.add('d-none');

            let val = convertStr2Num(value);
            // validation of the age input
            if (val >= 4 && val <= 100) {
                toggleErrorMessage('age', 'remove');
                local_storage.setItem('age', val);
            } else {
                local_storage.removeItem('age');
            }

            // in arabic ==> convert numbers to arabic
            if (typeof val === 'number' && !isNaN(val)) {
                if(local_storage.getItem('lang') === 'ar'){
                    document.getElementById('age').value = val.toLocaleString('ar-EG');
                }else {
                    document.getElementById('age').value = val;
                }
            }

            // filter list
            isAgeListOpen = true;
            openList(ageList.parentElement);
            ageListCurrentShown = [];
            document.querySelectorAll('#age-list > div').forEach(div => {
                if (div.dataset.value.indexOf(convertStr2Num(value) !== -1)
                    || div.innerText.indexOf(convertStr2Num(value)) !== -1) {
                    ageListCurrentShown.push(div); // for arrows event
                    div.classList.remove('d-none');
                } else {
                    div.classList.add('d-none');
                }
            });

            // re-scroll to the top
            scrollToSelected(ageList, ageList.querySelectorAll('div:not(.d-none)'), 0);

        } else {
            document.getElementById('age-placeholder-span').classList.remove('d-none');
            local_storage.removeItem('age');

            // close the list
            isAgeListOpen = false;
            closeList(ageList.parentElement);
        }

        beginBtnBgColor();

        // on change ==> select the div with same value
        if (ageList.querySelector('.checked')) ageList.querySelector('.checked').classList.remove('checked');
        if (ageList.querySelector(`[data-value = '${convertStr2Num(value)}']`)) {
            ageList.querySelector(`[data-value = '${convertStr2Num(value)}']`).classList.add('checked');
        }
    }
}

function _on_gender_change(value) {
    local_storage.setItem('gender', value);
    beginBtnBgColor();
}

function _on_tongue_change(e, value) {

    let val = value.replace(/ /g,'').toLocaleLowerCase();
    // in case that it's #not mathces, then will change UI and remove the item from local storage
    if (e.keyCode == 8 || e.keyCode == 229) {
        console.log(local_storage.getItem('tongue')&&toungue_lang_set[local_storage.getItem('tongue')]);
        console.log(local_storage.getItem('tongue'));
        
        if (local_storage.getItem('tongue') && toungue_lang_set[local_storage.getItem('tongue')] !== val) {
            console.log(toungue_lang_set[local_storage.getItem('tongue')],'العربية - البحرين');
            local_storage.removeItem('tongue');
            beginBtnBgColor();
            return;
        }
    }

    if (e.code !== 'Tab' && e.code !== 'Escape' && e.code !== 'ArrowUp' && e.code !== 'ArrowDown') {
        // hide the span (that do as a placeholder)
        if (value.trim()) document.getElementById('tongue-placeholder-span').classList.add('d-none');
        else document.getElementById('tongue-placeholder-span').classList.remove('d-none');

        // filter list
        let firstMatch, count = 0;

        // check that there's value on the input
        if (value.trim()) {
console.log(tounge_lang_pairs[val]);
            if (tounge_lang_pairs[val]) {
                toggleErrorMessage('tongue', 'remove');
                local_storage.setItem('tongue', tounge_lang_pairs[val]);
            } else {
                // open the list to filter it
                isTongueListOpen = true;
                openList(tongueList.parentElement);
                tongueListCurrentShown = [];
                // loop
                document.querySelectorAll('#tongue-list > div').forEach(div => {

                    // in case that user typed "hrvatski (Hrvatska)" or "hrvatski - Hrvatska"
                    // first get the div inner text which is the lang and the country
                    let innerDivsText = div.querySelector('div').innerText.replace(/ /g,'').toLocaleLowerCase();

                    // filter the items of the dropdown to show/ hide items
                    // and to select the first matched item to be blue
                 
                    if ( innerDivsText.indexOf(val) !== -1) {
                        div.classList.remove('d-none');
                        // on change ==> select the fisrt div with same value
                        if (!firstMatch) {
                            if (tongueList.querySelector('.checked')) tongueList.querySelector('.checked').classList.remove('checked');
                            firstMatch = div;
                            div.classList.add('checked');
                        }
                        tongueListCurrentShown.push(div);
                    } else {
                        count++;
                        div.classList.add('d-none');
                    }
                });
            }

            // there is no value in the input
        } else local_storage.removeItem('tongue');

        beginBtnBgColor();

        // close the list in case that there's no match or the input is empty
        if (count === tounge_lang_pairs.size || !value.trim()) {
            isTongueListOpen = false;
            closeList(tongueList.parentElement);
        }
    }
}

// ---- 6- on click on one of the items in the list (submenu)

function onClick_onageDiv(e) {
    document.getElementById('age-placeholder-span').classList.add('d-none');

    if (ageList.querySelector('.checked')) ageList.querySelector('.checked').classList.remove('checked');
    e.target.classList.add('checked');
    let dataAge = e.target.dataset.value;
    document.querySelector('#age').value = local_storage.getItem('lang') === 'ar' ? parseInt(dataAge).toLocaleString('ar-EG') : dataAge;
    local_storage.setItem('age', dataAge);

    beginBtnBgColor();
    toggleErrorMessage('age', 'remove');
}

tongueList.querySelectorAll(`#${tongueList.id} > div`).forEach(div => {
    div.addEventListener('click', function () {
        if (tongueList.querySelector('.checked')) tongueList.querySelector('.checked').classList.remove('checked');
        this.classList.add('checked');
        let text = div.querySelector('div').innerText;
        document.querySelector('#tongue').value = text;
        local_storage.setItem('tongue', this.dataset.value);
        document.getElementById('tongue-placeholder-span').classList.add('d-none');
        beginBtnBgColor();

        // in case the errors displayed ==> remove them
        toggleErrorMessage('tongue', 'remove');
    })
})

// error message that appear if the field data is wrong
function toggleErrorMessage(field, todo) {
    if (todo === 'add') {
        document.getElementById(`${field}Error`).classList.remove('not-visible');
        document.getElementById(field).classList.add('error-border');
    } else {
        document.getElementById(`${field}Error`).classList.add('not-visible');
        document.getElementById(field).classList.remove('error-border');

    }
}

// ------------------ functions of open and close the lists ------------------

function openList(parent) {
    parent.querySelector('.list-toggle-btn').classList.add('openBtn');
    parent.querySelector('.input-with-list').classList.remove('input-border');
    parent.querySelector('.list').classList.add('list-border');
}

function showTheListItems(parent) {

    let currentShow = [];

    // scroll to the current value
    if (local_storage.getItem(parent.querySelector('.input-with-list').id)) {
        let selected = 0, found = false;
        parent.querySelectorAll('.list > div').forEach(div => {
            div.classList.remove('d-none');
            currentShow.push(div);
            if (!found && !div.classList.contains('checked')) {
                selected++;
            }
            if (div.classList.contains('checked')) found = true;
        });
        scrollToSelected(parent.querySelector('.list'), parent.querySelectorAll('.list > div').length, selected);
    } else {
        parent.querySelectorAll('.list > div').forEach(div => {
            div.classList.remove('d-none');
            currentShow.push(div);
        });
    }

    if(parent.querySelector('.list').id === 'tongue-list') tongueListCurrentShown = currentShow;
    else ageListCurrentShown = currentShow;
}

function closeList(parent) {
    parent.querySelector('.list-toggle-btn').classList.remove('openBtn');
    parent.querySelector('.input-with-list').classList.add('input-border');
    parent.querySelector('.list').classList.remove('list-border');
    parent.querySelectorAll('.list > div').forEach(div => {
        div.classList.add('d-none');
    });
}

// ------------------ scrollTo function -----------------
function scrollToSelected(dropdown, numOfElemsm, selected) {
    const h = dropdown.scrollHeight;
    dropdown.scrollTo(0, (h / numOfElemsm) * selected - 20)
}

// ----------------------------------
// make it more accessible
// ----------------------------------

document.body.addEventListener('keydown', e => {
    if (e.code === 'Escape' || e.code === 'Tab') {
        isTongueListOpen = false;
        isAgeListOpen = false;
        closeList(document.getElementById('tongue-list').parentElement);
        closeList(document.getElementById('age-list').parentElement);
    }
})

// navigate on the list using arrows
document.getElementById('age').addEventListener('keyup', navigateOnList);
document.getElementById('tongue').addEventListener('keyup', navigateOnList);

let navigateWithArrowIndex = -1;
function navigateOnList(e) {
    
    if (e.code === 'ArrowUp' || e.code === 'ArrowDown') {
        let currentList = e.target.id === 'age' ? ageListCurrentShown : tongueListCurrentShown;
        let currentDOMList = e.target.id === 'age' ? ageList : tongueList;
        let curr, selected = -1;

        // get the current checked if exist
        for (let i = 0; i < currentList.length; i++) {
            const div = currentList[i];
            selected++;
            if (div.classList.contains('checked')) {
                curr = div;
                break;
            }
        }

        if(selected < 0) selected = 0;
        navigateWithArrowIndex = selected;

        // if user choose something before
        if (curr) curr.classList.remove('checked');

        let next;
        let scrollLength = currentList[0].clientHeight; //elemnts height

        if (e.code === 'ArrowUp') {
            navigateWithArrowIndex--;

            // scroll to the selected
            if(navigateWithArrowIndex >= 0) currentDOMList.scrollBy(0, -1 * (scrollLength));
            else currentDOMList.scrollBy(0, currentDOMList.scrollHeight);

            // change navigateWithArrowIndex value if it became nigative
            if(navigateWithArrowIndex < 0) navigateWithArrowIndex = currentList.length - 1;


        } else if (e.code === 'ArrowDown') {
            navigateWithArrowIndex++;
            if(navigateWithArrowIndex === currentList.length) navigateWithArrowIndex = 0;

            // scroll to the selected
            if(navigateWithArrowIndex) currentDOMList.scrollBy(0, scrollLength);
            else currentDOMList.scrollTo(0, 0);
        }

        next = currentList[navigateWithArrowIndex];
        next.classList.add('checked');

        // add values to the input
        if (e.target.id === 'age') {
            let dataAge = next.dataset.value;
            document.querySelector('#age').value = local_storage.getItem('lang') === 'ar' ? parseInt(dataAge).toLocaleString('ar-EG') : dataAge;

        } else if (e.target.id === 'tongue') {
            let text = next.querySelector('div').innerText;
            document.querySelector('#tongue').value = text;
        }

        // set value on localstorage
        local_storage.setItem(e.target.id, next.dataset.value);
        toggleErrorMessage(e.target.id, 'remove');
        document.getElementById(`${e.target.id}-placeholder-span`).classList.add('d-none');
    }
}