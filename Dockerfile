FROM php:7.4-apache

COPY apache.conf /etc/apache2/sites-available/000-default.conf
COPY php.ini /usr/local/etc/php/conf.d

RUN echo "Listen 8080" >> /etc/apache2/ports.conf
RUN a2enmod rewrite
RUN a2enmod headers

EXPOSE 8080

COPY ffprobe /usr/bin/ffprobe
COPY public/ /var/www/html/
RUN chown -R www-data:www-data /var/www/html/