#!/usr/bin/env bash

set -e
set -x

./docker_build.sh
./docker_push.sh
