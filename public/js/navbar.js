const navbaroggleBtn = document.querySelector('.navbar-toogle-icon');
navbaroggleBtn.addEventListener('click', () => {
    document.querySelector('.small-menu').classList.toggle('open');
    document.querySelector('#closed-menu-icon').classList.toggle('d-none');
    document.querySelector('#opened-menu-icon').classList.toggle('d-none');
})

document.body.addEventListener('click', (e) => {
    if (window.innerWidth < 1156) {
        let shouldClose = true;

        // check if the nav toggle btn clicked
        if(e.target.id === 'closed-menu-icon' || e.target.parentElement.id === 'closed-menu-icon' 
        || e.target.id === 'opened-menu-icon' || e.target.parentElement.id === 'opened-menu-icon'){
            shouldClose = false;
        }

        // if anything clicked except the nav toggle btn
        if (shouldClose) {
            closeSmallNavBar();
        }
    }
})

function closeSmallNavBar() {
    document.querySelector('.small-menu').classList.remove('open');
    document.querySelector('#closed-menu-icon').classList.remove('d-none');
    document.querySelector('#opened-menu-icon').classList.add('d-none');
}

function about_page_change_language(lang) {
    // to change other pages languages when the user go to
    change_language(lang);

    // to redirect the user only if he want to change the language
    const href = window.location.href;
    const currentPage = href.substr(href.lastIndexOf('/') + 1, href.length);
    if (currentPage !== `about_${lang}.html`) {
        window.location.href = `./about_${lang}.html`;
    }
}

