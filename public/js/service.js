export { upload_recording, dataset_meta };

const local_storage = window.localStorage;
const speaker_id = local_storage.getItem('id');
const age = local_storage.getItem('age');
const gender = local_storage.getItem('gender');
const tongue = local_storage.getItem('tongue');

function dataset_meta(on_result)
{
    http_get('api/dataset_meta', 
        // on success
        (response) => {
            on_result(JSON.parse(response));
        },
        // on failure
        (error) => {
            console.log(error);
        });
}

function upload_recording(blob, sura, aya, on_upload_complete)
{
    // Define the FileReader which is able to read the contents of Blob
    var reader = new FileReader();

    // The magic always begins after the Blob is successfully loaded
    reader.onload = function () {
        console.log("converted wav to base64");
        // Since it contains the Data URI, we should remove the prefix and keep only Base64 string
        var b64 = reader.result.replace(/^data:.+;base64,/, '');
        upload_b64(b64, sura, aya, on_upload_complete);
    };

    // Since everything is set up, let’s read the Blob and store the result as Data URI
    reader.readAsDataURL(blob);

}

function http_request(url, method, data, onsuccess, onfailure)
{
    var http = new XMLHttpRequest();
    http.open(method, url, true); // true for asynchronous
    http.send(data);

    http.onreadystatechange = function()
    {
        if (http.readyState == 4 && http.status == 200)
        {
            onsuccess(http.responseText);
            return;
        }
        else if(http.readyState == 4 && http.status != 200)
        {
            onfailure(this.status, this.responseText);
        }
    }
}

function http_post(url, data, onsuccess, onfailure)
{
    http_request(url, "POST", data, onsuccess, onfailure);
}

function http_get(url, onsuccess, onfailure)
{
    http_request(url, "GET", null, onsuccess, onfailure);
}

function _on_upload_success(response)
{
    console.log("response received");
    console.log("response: " + response);
    console.log("Server responded with 200 success");
}

function _on_upload_failure(status, response)
{
    console.log("Request failed, server responded with: ");
    console.log(response);
    console.log("response code: " + status);
}

function upload_json(json, on_upload_complete)
{
    console.log("sending request");

    http_post('api/store', json,
        (response) => {
            _on_upload_success(response);
            on_upload_complete();
        },
        (status, response) => {

            _on_upload_failure(status, response);

            if(status == 503)
            {
                console.log("attempting reupload...");
                upload_json(json, on_upload_complete);
                return;
            }

            on_upload_complete();
        });
}

function upload_b64(b64, sura, aya, on_upload_complete)
{
    var request_obj = {
        speaker_id: speaker_id,
        audio: b64,
        sura: sura,
        aya: aya,
        tongue: tongue,
        gender: gender,
        age: age
    };

    var json = JSON.stringify(request_obj);

    upload_json(json, on_upload_complete);
}