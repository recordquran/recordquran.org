<?php

require_once "utils.php";
require_once "router.php";
require_once "store.php";
require_once "dataset.php";
require_once __DIR__ . '/vendor/autoload.php';


$request_size = (int) $_SERVER['CONTENT_LENGTH'];

// 50 MB
if($request_size > 52428800)
{
    http_response_code(400);
    user_error_log('400 request too big');
    die();
}

post('store', 'store');
get('dataset_meta', 'dataset_meta');

http_response_code(404);
user_error_log('404 not found');