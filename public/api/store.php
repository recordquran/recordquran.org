<?php

require_once "quran_map.php";
require_once "utils.php";
require_once "config.php";

// Imports the Cloud Storage client library.
use \Google\Cloud\Storage\StorageClient;

/**
 * Upload a file.
 *
 * @param string $bucketName the name of your Google Cloud bucket.
 * @param string $objectName the name of the object.
 * @param string $source the path to the file to upload.
 *
 * @return Psr\Http\Message\StreamInterface
 */
function upload_wave($projectId, $bucketName, $objectName, $source)
{
    $config = [
        'projectId' => $projectId,
    ];

    $storage = null;

    try
    {
        $storage = new StorageClient($config);
    }
    catch(Exception $e)
    {
        error_log("Could not create StorageClient: {$e->getMessage()}");
        echo("500 internal server error");
        http_response_code(500);
        die();
    }

    $file = fopen($source, 'r');
    $bucket = $storage->bucket($bucketName);

    try
    {
        $object = $bucket->upload($file, [
            'name' => $objectName
        ]);
    }
    catch(Exception $e)
    {
        error_log("Could not upload to bucket: {$e->getMessage()}");
        echo("500 internal server error");
        http_response_code(500);
        die();
    }
    
    
    $basename = basename($source);
    error_log("Uploaded {$basename} to gs://{$bucketName}/{$objectName}" . PHP_EOL);
}

function validate_request($request_data)
{
    if(!$request_data->audio)
    {
        http_response_code(400);
        user_error_log('400 audio not found');
        die();
    }

    if(!$request_data->speaker_id)
    {
        http_response_code(400);
        user_error_log('400 speaker_id not found');
        die();
    }

    // makes sure the speaker id is alphanumeric with dashes and nothing else
    if (!preg_match('/^[A-Za-z0-9-]+$/', $request_data->speaker_id))
    {
        http_response_code(400);
        user_error_log('400 invalid characters in speaker_id');
        die();
    }

    if(strlen($request_data->speaker_id) > 50)
    {
        http_response_code(400);
        user_error_log('400 speaker_id is more than 50 characters');
        die();
    }

    if(!$request_data->age)
    {
        http_response_code(400);
        user_error_log('400 age not found');
        die();
    }

    if($request_data->age < 4 || $request_data->age > 100)
    {
        http_response_code(400);
        user_error_log('400 age is not in the acceptable range');
        die();
    }

    if(!$request_data->tongue)
    {
        http_response_code(400);
        user_error_log('400 tongue not found');
        die();
    }

    $acceptable_tongues = [
        "af-ZA", "sq-AL", "am-ET", "ar-DZ", "ar-BH", "ar-EG", "ar-IQ", 
        "ar-IL", "ar-JO", "ar-KW", "ar-LB", "ar-MA", "ar-OM", "ar-QA", 
        "ar-SA", "ar-PS", "ar-TN", "ar-AE", "ar-YE", "hy-AM", "az-AZ", 
        "eu-ES", "bn-BD", "bn-IN", "bs-BA", "bg-BG", "my-MM", "ca-ES", 
        "yue-Hant-HK", "zh", "zh-TW", "hr-HR", "cs-CZ", "da-DK", "nl-BE", 
        "nl-NL", "en-AU", "en-CA", "en-GH", "en-HK", "en-IN", "en-IE", 
        "en-KE", "en-NZ", "en-NG", "en-PK", "en-PH", "en-SG", "en-ZA", 
        "en-TZ", "en-GB", "en-US", "et-EE", "fil-PH", "fi-FI", "fr-BE", 
        "fr-CA", "fr-FR", "fr-CH", "gl-ES", "ka-GE", "de-AT", "de-DE", 
        "de-CH", "el-GR", "gu-IN", "iw-IL", "hi-IN", "hu-HU", "is-IS", 
        "id-ID", "it-IT", "it-CH", "ja-JP", "jv-ID", "kn-IN", "km-KH", 
        "ko-KR", "lo-LA", "lv-LV", "lt-LT", "mk-MK", "ms-MY", "ml-IN", 
        "mr-IN", "mn-MN", "ne-NP", "no-NO", "fa-IR", "pl-PL", "pt-BR", 
        "pt-PT", "pa-Guru-IN", "ro-RO", "ru-RU", "sr-RS", "si-LK", "sk-SK", 
        "sl-SI", "es-AR", "es-BO", "es-CL", "es-CO", "es-CR", "es-DO", 
        "es-EC", "es-SV", "es-GT", "es-HN", "es-MX", "es-NI", "es-PA", 
        "es-PY", "es-PE", "es-PR", "es-ES", "es-US", "es-UY", "es-VE", 
        "su-ID", "sw-KE", "sw-TZ", "sv-SE", "ta-IN", "ta-MY", "ta-SG", 
        "ta-LK", "te-IN", "th-TH", "tr-TR", "uk-UA", "ur-IN", 
        "ur-PK", "uz-UZ", "vi-VN", "zu-ZA"
    ];

    if(!in_array($request_data->tongue, $acceptable_tongues))
    {
        http_response_code(400);
        user_error_log('400 tongue is not in the acceptable range');
        die();
    }

    if(!$request_data->gender)
    {
        http_response_code(400);
        user_error_log('400 gender not found');
        die();
    }

    if($request_data->gender != "m" && $request_data->gender != "f")
    {
        http_response_code(400);
        user_error_log('400 gender can only be m or f');
        die();
    }

    if(!$request_data->sura)
    {
        http_response_code(400);
        user_error_log('400 sura not found');
        die();
    }

    if($request_data->sura < 1 || $request_data->sura > 114)
    {
        http_response_code(400);
        user_error_log('400 sura is not in the acceptable range');
        die();
    }

    if(!$request_data->aya)
    {
        http_response_code(400);
        user_error_log('400 aya not found');
        die();
    }

    global $suras_ayat;

    if($request_data->aya < 1 || $request_data->aya > $suras_ayat[$request_data->sura])
    {
        http_response_code(400);
        user_error_log('400 aya is not in the acceptable range');
        die();
    }
}

function on_bad_wave($path, $reason)
{
    http_response_code(400);
    user_error_log('400 bad wave ' . $reason);
    unlink($path);
    die();
}

function duration_text_to_seconds($duration)
{
    if (!preg_match('/\:/', $duration))
    {
        return null;
    }

    $parts = explode(':', $duration);

    if (!count($parts) == 3)
    {
        return null;
    }

    $hours = (int)$parts[0];
    $minutes = (int)$parts[1];
    $seconds = (float)$parts[2];

    return $seconds + ($minutes * 60) + ($hours * 3600);
}

function extract_duration($ffprobe_output)
{
    $ffprobe_output = explode(' ', $ffprobe_output);
    
    $duration_next = false;

    foreach($ffprobe_output as $i)
    {
        if($i == "Duration:")
        {
            $duration_next = true;
            continue;
        }

        if($duration_next)
        {
            string_pop($i);
            return duration_text_to_seconds($i);
        }
    }

    return null;
}

function extract_audio_type($ffprobe_output)
{
    $ffprobe_output = explode(' ', $ffprobe_output);
    
    $audio_next = false;

    foreach($ffprobe_output as $i)
    {
        if($i == "Audio:")
        {
            $audio_next = true;
            continue;
        }

        if($audio_next)
        {
            string_pop($i);
            return $i;
        }
    }

    return null;
}

function store()
{
    //Receive the RAW post data.
    $content = trim(file_get_contents("php://input"));
    $decoded = json_decode($content, false);

    $audio = base64_decode($decoded->audio);

    validate_request($decoded);

    $prefix = uniqid();

    $name = "{$prefix}_{$decoded->speaker_id}_{$decoded->age}_{$decoded->tongue}_{$decoded->gender}_{$decoded->sura}_{$decoded->aya}.opus";

    $path = "/tmp/" . $name;

    file_put_contents($path, $audio);

    $output = shell_exec('ffprobe ' . $path . ' 2>&1');

    $audio_type = extract_audio_type($output);
    $duration = extract_duration($output);

    if(!$audio_type || $audio_type != "opus")
    {
        on_bad_wave($path, "wave is not opus or corrupted.");
    }

    if(!$duration || $duration < 3 || $duration > 600)
    {
        on_bad_wave($path, "wave is too short, too long or corrupted.");
    }

    error_log('Storing...\n');
    upload_wave(project_name(), bucket_name(), $name, $path);

    unlink($path);
    
    http_response_code(200);
}