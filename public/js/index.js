// to fix the conflict between it's functionality on record and without recording

// 1- add nextAya() function as eventListner to next_btn
// 2- on record remove the nextAya() function and add _on_next_btn_press as eventListner to next_btn
// 3- on stop recording remove _on_next_btn_press and add nextAya() as eventListner to next_btn 
// 4- to fix the scope of recording_ctx, declare it above the prepare_context_and_record()

// ------------------------------------------------------------------------------

import {
    change_language,
    populate_suras,
    populate_numbers_dropdown,
    finished_recording_message,
    upload_in_progress_message,
    arabic_digits,
    available_languages,
    convertStr2Num,
    set_lang_from_url
} from "./languages.js";

import { get_aya_text as get_aya_text_ } from "./all_ayat.js";

import { upload_recording } from "./service.js";

import { start_recording, stop_recording } from "./record.js";

import { stripTashkeel } from "./tashkeel.js";

const local_storage = window.localStorage;

const record_btn = document.getElementById("record_btn");
const stop_btn = document.getElementById("stop_btn");
const next_btn = document.getElementById("next_btn");
const prev_btn = document.getElementById("prev_btn");
const timer = document.getElementById("timer");
const minutes = document.getElementById("minutes");
const seconds = document.getElementById("seconds");
const sura = document.getElementById("sura-tabel-content");
const aya = document.getElementById("ayat-tabel-content");
const aya_text = document.getElementById("aya_text");
const just_a_sec = document.getElementById("just_a_sec");

const age = Number(local_storage.getItem('age'));
const gender = local_storage.getItem('gender');
const tongue = local_storage.getItem('tongue');
const speaker_id = local_storage.getItem('id');

let selected_sura = Number(local_storage.getItem('sura'));
let selected_aya = Number(local_storage.getItem('aya'));

let isSuraInputChanged = false;
let isAyaInputChanged = false;

window.AudioContext = window.AudioContext || window.webkitAudioContext;
window.URL = window.URL || window.webkitURL;

if (selected_sura === 0 ||
    selected_aya === 0) {
    selected_sura = 1;
    selected_aya = 1;
}

var recording = false;

if (age === 0 ||
    gender === null || gender === "" ||
    tongue === null || tongue === "")
{    
    set_lang_from_url();
    window.location.href = "register.html";
}

function recording_update_ui() {
    if (recording) {
        timer.classList.remove('d-none');
        setTimeout(() => {
            prev_btn.classList.add('stop_prev_btn');
            record_btn.classList.add('d-none');
            stop_btn.classList.remove('d-none');
            document.getElementById('before-recoding-message').classList.add('d-none');
            document.getElementById('while-recoding-message').classList.remove('d-none');
            // to fix the conflict between it's functionality on record and without recording
            next_btn.removeEventListener('click', nextAya);
            next_btn.addEventListener('click', nextBtnOnRecord);
        }, 500);
    } else {
        timer.classList.add('d-none')
        prev_btn.classList.remove('stop_prev_btn');
        record_btn.classList.remove('d-none');
        stop_btn.classList.add('d-none');
        document.getElementById('before-recoding-message').classList.remove('d-none');
        document.getElementById('while-recoding-message').classList.add('d-none');
        // to fix the conflict between it's functionality on record and without recording
        next_btn.addEventListener('click', nextAya);
        next_btn.removeEventListener('click', nextBtnOnRecord);
    }
}

function pad(val) {
    var valString = val + "";
    let isArabic = local_storage.getItem('lang') === 'ar';
    if (!isArabic && valString.length < 2) {
        return "0" + valString;
    } else if (isArabic && valString.length <= 7) {
        return arabic_digits("0") + valString;
    }
    else {
        return valString;
    }
}

function update_timer(timer_ctx) {
    timer_ctx.total_seconds++;
    if (local_storage.getItem('lang') === 'ar') {
        seconds.innerHTML = pad(arabic_digits(timer_ctx.total_seconds % 60));
        minutes.innerHTML = pad(arabic_digits(parseInt(timer_ctx.total_seconds / 60)));
    } else {
        seconds.innerHTML = pad(timer_ctx.total_seconds % 60);
        minutes.innerHTML = pad(parseInt(timer_ctx.total_seconds / 60));
    }
}

function start_timer(timer_ctx) {
    timer_ctx.total_seconds = 0;
    timer_ctx.timer_interval = setInterval(update_timer, 1000, timer_ctx);
}

function stop_timer(timer_ctx) {
    clearInterval(timer_ctx.timer_interval);
    if (local_storage.getItem('lang') === 'ar') {
        minutes.innerHTML = arabic_digits("00");
        seconds.innerHTML = arabic_digits("00");
    } else {
        minutes.innerHTML = "00";
        seconds.innerHTML = "00";
    }
}

function _on_next_btn_press(recording_ctx) {
    setTimeout(() => {
        on_record_complete(recording_ctx);
    }, 500);

    just_a_sec.hidden = false;
    aya_text.hidden = true;

    stop_timer(recording_ctx.timer_ctx);

    // has to be called after selected_sura/aya
    // have been changed
    nextAya();
    prepare_context_and_record();
    return;
}

next_btn.addEventListener('click', nextAya);
prev_btn.addEventListener('click', prevAya);

function nextAya() {
    if (selected_aya + 1 > suras_ayat[selected_sura]) {
        if (selected_sura + 1 > 114) {
            selected_sura = 1;
            select_option(sura, selected_sura);
            _on_sura_change(true, 1);
            select_option(aya, selected_aya);
            return;
        }

        selected_sura++;
        select_option(sura, selected_sura);
        _on_sura_change(true, selected_sura);
        select_option(aya, selected_aya);
        return;
    }

    selected_aya++;
    select_option(aya, selected_aya);
    _on_aya_change(selected_aya, true);
    return;
}

function prevAya() {
    if (!recording) {
        if (selected_aya - 1 < 1) {
            if (selected_sura - 1 < 1) {
                selected_sura = 114;
                select_option(sura, selected_sura);
                _on_sura_change(!recording, 114, suras_ayat[selected_sura]);
                select_option(aya, selected_aya);
                return;
            }

            selected_sura--;
            select_option(sura, selected_sura);
            _on_sura_change(!recording, selected_sura, suras_ayat[selected_sura]);
            select_option(aya, selected_aya);
            return;
        }

        selected_aya--;
        select_option(aya, selected_aya);
        _on_aya_change(selected_aya, !recording);
        return;
    }
}

var recording_ctx; // to fix the conflict between it's functionality on record and without recording
function prepare_context_and_record() {
    recording_ctx = {
        recorder: null,

        sura: selected_sura,
        aya: selected_aya,

        timer_ctx: {
            timer_interval: null,
            total_seconds: 0
        }
    };

    stop_btn.onclick = () => {
        _on_stop_btn_press(recording_ctx);
    };

    recording_ctx.recorder = start_recording(() => {
        recording = true;
        just_a_sec.hidden = false;
        aya_text.hidden = true;
        setTimeout(() => {
            just_a_sec.hidden = true;
            aya_text.hidden = false;
            recording_update_ui(true);
            start_timer(recording_ctx.timer_ctx);
        }, 500);
    });
}

function nextBtnOnRecord() {
    _on_next_btn_press(recording_ctx);
}

function _on_record_btn_press() {

    clear_message();
    clear_recordings_count();
    clear_uploaded_recordings();
    prepare_context_and_record();

    // there's change in UI happen once the permission taken from user
    // it's in the start_recording()
}

function update_message() {
    const message = document.getElementById('message');
    const recordings_count = Number(local_storage.getItem('recordings_count'));
    const uploaded_recordings = Number(local_storage.getItem('uploaded_recordings'));

    if (recordings_count === uploaded_recordings) {
        const total_recordings_count = local_storage.getItem('total_recordings_count');
        const message_text = finished_recording_message(recordings_count, total_recordings_count);
        message.innerHTML = `<p>${message_text}</p>`;
    }
    else {
        const message_text = upload_in_progress_message(uploaded_recordings, recordings_count);
        message.innerHTML = `<p>${message_text}</p>`;
    }
}

function clear_message() {
    const message = document.getElementById('message');
    message.innerHTML = "";
}

function _on_stop_btn_press(recording_ctx) {
    recording = false;

    recording_update_ui(false);
    stop_timer(recording_ctx.timer_ctx);
    on_record_complete(recording_ctx);
    update_message();
}

function get_aya_text() {
    aya_text.innerHTML = get_aya_text_(selected_sura, selected_aya);
}

function increment_local_storage_key(key) {
    let value = local_storage.getItem(key);

    if (value == null || value == '') {
        value = 0;
    }

    value = Number(value);
    value++;

    local_storage.setItem(key, value);
}

function increment_recordings_count() {
    increment_local_storage_key('recordings_count');
    increment_local_storage_key('total_recordings_count');
}

function clear_recordings_count() {
    local_storage.setItem('recordings_count', 0);
}

function clear_uploaded_recordings() {
    local_storage.setItem('uploaded_recordings', 0);
}

function _on_upload_complete() {
    increment_local_storage_key('uploaded_recordings');

    if (!recording) {
        update_message();
    }
}

function on_record_complete(recording_ctx) {
    stop_recording(recording_ctx.recorder, (blob) => {
        upload_recording(blob, recording_ctx.sura, recording_ctx.aya, _on_upload_complete);
    });

    increment_recordings_count();
}

function _on_aya_change(aya_number, condition) {
    if (condition) {
        selected_aya = Number(aya_number);
        get_aya_text();
        local_storage.setItem('aya', aya_number);
        document.getElementById('aya-search').value = (local_storage.getItem('lang') === 'ar')
            ? selected_aya.toLocaleString('ar-EG') : selected_aya;
    }
}

function _on_sura_change(condition, sura_number, aya_number) {
    if (condition) {
        selected_sura = Number(sura_number);
        populate_numbers_dropdown(aya, 1, suras_ayat[selected_sura]);
        local_storage.setItem('sura', sura_number);
        _on_aya_change(aya_number || 1, true);
        scrollToSelected(aya, aya.querySelectorAll('p').length, 1);

        let suras = eval(available_languages[local_storage.getItem('lang')].sura_list)
        document.getElementById('sura-search').value = suras[selected_sura];
        select_option(aya, selected_aya);
    }
}

function select_option(dropdown, value) {
    var opts = dropdown.querySelectorAll('.tabel-options');
    for (var opt, j = 0; opt = opts[j]; j++) {
        if (opt.dataset.value == value.toString()) {
            dropdown.selectedIndex = j;
            // UI 
            if (dropdown.querySelector('.selected')) dropdown.querySelector('.selected').classList.remove('selected');
            dropdown.querySelector(`[data-value='${value}']`).classList.add('selected');
            scrollToSelected(dropdown, opts.length, j)
            break;
        }
    }
}

function options_init(lang) {
    populate_suras(lang);
    populate_numbers_dropdown(aya, 1, suras_ayat[selected_sura]);
    select_option(sura, selected_sura);
    select_option(aya, selected_aya);

    let suras = eval(available_languages[lang].sura_list)
    document.getElementById('sura-search').value = suras[selected_sura];
    document.getElementById('aya-search').value = (local_storage.getItem('lang') === 'ar')
        ? selected_aya.toLocaleString('ar-EG') : selected_aya;
}

function _on_language_change(lang) {
    lang = change_language(lang);
    options_init(lang);
}

window._on_language_change = _on_language_change;
window._on_record_btn_press = _on_record_btn_press;
window._on_sura_change = _on_sura_change;
window._on_aya_change = _on_aya_change;
window.changeSelectedOptionUI = changeSelectedOptionUI;
window.onSelectAyaFromDropDownClicked = onSelectAyaFromDropDownClicked;
window.onSelectSuraFromDropDown = onSelectSuraFromDropDown;
window.closeAyaDropDownOnMob = closeAyaDropDownOnMob;
window.closeSuraDropDownOnMop = closeSuraDropDownOnMop;

options_init(window.localStorage.getItem('lang'));
get_aya_text();

function onSelectAyaFromDropDownClicked(val) {
    isAyaInputChanged = false;
    _on_aya_change(val, !recording);
    changeSelectedOptionUI(`aya-number`, val);

    // on mobile => close the list
    closeAyaDropDownOnMob();
}

function onSelectSuraFromDropDown(val) {
    isSuraInputChanged = false;
    _on_sura_change(!recording, val);
    changeSelectedOptionUI(`sura-name`, val);

    // on mobile => close the list
    closeSuraDropDownOnMop();
}

// ----------------------------------
// UI
// ----------------------------------

function changeSelectedOptionUI(className, num) {
    
    if (!recording) {
        let dropdown = document.querySelector(`.${className}`).parentElement;
        let length = dropdown.querySelectorAll(`.${className}`).length;
        removeAllNoneDisplayedElem(dropdown);

        document.querySelector(`.${className}.selected`).classList.remove('selected');
        let selected = document.querySelector(`.${className}[data-value='${num}']`);
        selected.classList.add('selected');

        // scroll to selected
        scrollToSelected(dropdown, length, parseInt(selected.dataset.value));
    }
}

function scrollToSelected(dropdown, numOfElemsm, selected) {
    removeAllNoneDisplayedElem(dropdown);
    const h = dropdown.scrollHeight;
    dropdown.scrollTo(0, (h / numOfElemsm) * selected - 50)
}

function removeAllNoneDisplayedElem(dropdown) {
    if (dropdown.querySelector('.d-none')) {
        dropdown.querySelectorAll('.d-none').forEach(elem => {
            elem.classList.remove('d-none');
        })
    }
}

// ---------------------------------
// search functionality
// ---------------------------------

const suraSearchForm = document.getElementById('sura-search-form');
const ayaSearchForm = document.getElementById('aya-search-form');

// ===============> sura
suraSearchForm['sura-search'].addEventListener('keyup', e => {
    e.preventDefault();
    // 
    if (!recording && e.keyCode !== 13 && e.code !== 'Tab' && e.key !== 'Shift' && e.key !=='Enter') { // press Enter meain submit / press tab mean focus
    console.log(e.keyCode);
      
        isSuraInputChanged = true;
        let matchedSuras = searchForSura();
        if (matchedSuras) {
            sura.querySelectorAll('p').forEach(p => {
                if (matchedSuras[p.dataset.value]) {
                    p.classList.remove('d-none');
                } else {
                    p.classList.add('d-none');
                }
            });
            sura.scrollTo(0,0);
        } else {
            removeAllNoneDisplayedElem(sura)
        }

        // on mobile open this list and close the other list
        document.getElementById('toggle-sura-tabel').classList.add('clicked');
        document.getElementById('sura-tabel-content').classList.add('open');
        closeAyaDropDownOnMob();
    }
})

function searchForSura() {
    
    const suraName = suraSearchForm['sura-search'].value;


    if (suraName.trim()) {
        const lang = local_storage.getItem('lang');
        let suras = eval(available_languages[lang].sura_list);
        let data = {}
        for (let i = 0; i < suras.length; i++) {
            if (suras[i].toLocaleLowerCase().indexOf(stripTashkeel(suraName.toLocaleLowerCase())) !== -1) {
                data[i] = suras[i]
                console.log(suras[i]);
                console.log(data[i]);
    // select_option(val, selected_aya);


            }
        }
        return data;
    }
    return;
}

suraSearchForm.addEventListener('submit', e => {
    e.preventDefault();
    if (!recording) {
        // get matched suras
        isSuraInputChanged = false;
        let matchedSuras = searchForSura();
        if (matchedSuras) {
            let suraIndex, searchFor;
            for (let index in matchedSuras) {
                suraIndex = index;
                searchFor = matchedSuras[index];
                break;
            }

            // search for the first one
            if (suraIndex) {
                suraSearchForm['sura-search'].value = searchFor;
                _on_sura_change(!recording, suraIndex, 1);
                scrollToSelected(sura, sura.querySelectorAll('.sura-name').length, suraIndex);
                sura.querySelector('.sura-name.selected').classList.remove('selected');
                sura.querySelector(`.sura-name[data-value='${suraIndex}']`).classList.add('selected');
            }
        }

        // close the list
        closeSuraDropDownOnMop();
    }
})

// ==============> aya

ayaSearchForm.addEventListener('submit', e => {
    e.preventDefault();
    isAyaInputChanged = false;
    let val = getAyaNumVal();
    searchByAya(val);
})

function ayaSearchChange(e) {
    if (e.keyCode !== 13 && e.code !== 'Tab' && e.key !== 'Shift') { // press Enter meain submit / press tab mean focus
        isAyaInputChanged = true;
        let val = getAyaNumVal();
        if (val) val += ''; //convert it to string

        if (val) {
            aya.querySelectorAll('p').forEach(p => {
                if ((p.dataset.value).indexOf(val) !==-1) {
                    p.classList.remove('d-none');
                } else {
                    p.classList.add('d-none');
                }
            });
            aya.scrollTo(0,0);
        } else { //in case the input empty
            removeAllNoneDisplayedElem(aya);
        }

        // on mobile open this list and close the other list
        document.getElementById('toggle-ayat-tabel').classList.add('clicked');
        document.getElementById('ayat-tabel-content').classList.add('open');
        closeSuraDropDownOnMop();
    }
}

function getAyaNumVal() {
    if (!recording && ayaSearchForm['aya-search'].value.trim()) {
        return convertStr2Num(ayaSearchForm['aya-search'].value);
    }
}

function searchByAya(val) {
    if (val && val <= suras_ayat[selected_sura]) {
        _on_aya_change(val, !recording);
        scrollToSelected(aya, aya.querySelectorAll('.aya-number').length, val);
        if(aya.querySelector('.aya-number.selected')) aya.querySelector('.aya-number.selected').classList.remove('selected');
        aya.querySelector(`.aya-number[data-value='${val}']`).classList.add('selected');
    } else { // in a scenario like typing 55 in Al-Fatihah ==> just ignore the number and display all the list
        removeAllNoneDisplayedElem(aya)
    }
}

document.getElementById('aya-search').addEventListener('keyup', ayaSearchChange);

// -----------------------------------------------------------------------------------
// in case user delete the aya number from the aya input or sura name ==> get them back
// -----------------------------------------------------------------------------------

const ids = {
    'sura-search': 'sura-search',
    'aya-search': 'aya-search',
    'sura-tabel-content': 'sura-tabel-content',
    'ayat-tabel-content': 'ayat-tabel-content',
    'sura-search-submit': 'sura-search-submit',
}

const toggleBtnsIds = {
    'toggle-sura-tabel': 'toggle-sura-tabel',
    'toggle-ayat-tabel': 'toggle-ayat-tabel'
}

document.body.addEventListener('click', e => {
    if (!ids[e.target.id] && !ids[e.target.parentElement.id]) {
        // sura
        if (isSuraInputChanged) {
            const suraName = suraSearchForm['sura-search'];
            const lang = local_storage.getItem('lang');
            let suras = eval(available_languages[lang].sura_list);
            suraName.value = suras[selected_sura];
            scrollToSelected(sura, sura.querySelectorAll('p').length, selected_sura);
        }

        // aya
        if (isAyaInputChanged) {
            const ayaName = ayaSearchForm['aya-search'];
            ayaName.value = local_storage.getItem('lang') === 'ar' ? selected_aya.toLocaleString('ar-EG') : selected_aya;
            scrollToSelected(aya, aya.querySelectorAll('p').length, selected_aya);
        }
        
        const suraToggleBtn = document.querySelector('#toggle-sura-tabel');
        if(!toggleBtnsIds[e.target.id] && !toggleBtnsIds[e.target.parentElement.id] && suraToggleBtn.classList.contains('clicked')){
            // currently it's open, so close it
            closeSuraDropDownOnMop();
        }

        const ayatToggleBtn = document.querySelector('#toggle-ayat-tabel');
        if(!toggleBtnsIds[e.target.id] && !toggleBtnsIds[e.target.parentElement.id] && ayatToggleBtn.classList.contains('clicked')){
            // currently it's open, so close it
            closeAyaDropDownOnMob();
        }
    }
})

// --------------------
// helper functions
// --------------------

function closeAyaDropDownOnMob() {
    document.getElementById('toggle-ayat-tabel').classList.remove('clicked');
    document.getElementById('ayat-tabel-content').classList.remove('open');
}

function closeSuraDropDownOnMop() {
    document.getElementById('toggle-sura-tabel').classList.remove('clicked');
    document.getElementById('sura-tabel-content').classList.remove('open');
}

// -----------------
// on lang change
// -----------------

window.onLangChangOnRecordPage = onLangChangOnRecordPage;

function onLangChangOnRecordPage(){
    // change the record message
    const message = document.getElementById('message');
    if(message && message.innerHTML){
        update_message()
    }
}

// ----------------------------------
// make it more accessible
// ----------------------------------

sura.addEventListener('keydown', navigateUsingArrows);
aya.addEventListener('keydown', navigateUsingArrows);

function navigateUsingArrows(e) {
    e.preventDefault();

    if(!recording){
        if (e.code === 'ArrowUp' || e.code === 'ArrowDown') {
            let collection = e.target.querySelectorAll('p');
            let curr = e.target.querySelector('.selected');
            curr.classList.remove('selected');
            let next, scrollLength = curr.clientHeight + 12;
    
            if (e.code === 'ArrowUp') {
                if (curr.previousElementSibling) {
                    next = curr.previousElementSibling;
                    e.target.scrollBy(0, -1 * (scrollLength));
                } else {
                    next = collection[collection.length - 1];
                    e.target.scrollBy(0, e.target.scrollHeight);
                }
            }
    
            if (e.code === 'ArrowDown') {
                if (curr.nextElementSibling) {
                    next = curr.nextElementSibling;
                    e.target.scrollBy(0, scrollLength);
                } else {
                    next = collection[0];
                    e.target.scrollTo(0, 0);
                }
            }
    
            next.classList.add('selected');
            let val = next.dataset.value;
            if (e.target.id === 'sura-tabel-content') {
                _on_sura_change(!recording, val);
                changeSelectedOptionUI(`sura-name`, val);
            } else {
                _on_aya_change(val, !recording);
                changeSelectedOptionUI(`aya-number`, val);
            }
        }
    }

}


document.body.addEventListener('keydown', e => {
    if (e.code === 'Tab') {
        //add all elements we want to include in our selection
        var focussableElements = 'a:not([disabled]), button:not([disabled]), input[type=text]:not([disabled]), [tabindex]:not([disabled]):not([tabindex="-1"])';
        console.log('hi');
        var focussable = Array.prototype.filter.call(document.querySelectorAll(focussableElements),
            function (element) {
                //check for visibility while always include the current activeElement 
                return element.offsetWidth > 0 || element.offsetHeight > 0 || element === document.activeElement
            });
        var index = focussable.indexOf(document.activeElement);
        if (index > -1) {
            if (e.target.id === 'sura-tabel-content' || e.target.id === 'ayat-tabel-content') {
                if (e.shiftKey) index--;
                else index++;
            }
            if (index < 0) index = 0;
            var nextElement = focussable[index] || focussable[0];
            nextElement.focus();
        }
    }
})