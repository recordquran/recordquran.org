// ----------------------------------------------
// functions in case user search using tashkeel
// ----------------------------------------------

const CHARCODE_SHADDA = 1617;
const CHARCODE_SUKOON = 1618;
const CHARCODE_SUPERSCRIPT_ALIF = 1648;
const CHARCODE_TATWEEL = 1600;
const CHARCODE_ALIF = 1575;

function isCharTashkeel(letter) {
    if (typeof (letter) == "undefined" || letter == null) return false;

    const code = letter.charCodeAt(0);
    //1648 - superscript alif
    //1619 - madd: ~
    return (code == CHARCODE_TATWEEL || code == CHARCODE_SUPERSCRIPT_ALIF || code >= 1612 && code <= 1631); //tashkeel
}

function stripTashkeel(input) {
    let output = "";
    //todo consider using a stringbuilder to improve performance
    for (let i = 0; i < input.length; i++) {
        const letter = input.charAt(i);
        if (!isCharTashkeel(letter)) {
            //tashkeel
            output += letter;
        }
    }
    return output;
}

export {stripTashkeel}